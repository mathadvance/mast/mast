# MAST Materials

**This is a script for internal use**, meaning that a) to get anything meaningful you have to have access and b) your development environment has to be set up properly.

Your development environment must have LaTeX with Asymptote and latexmk properly configured, and it also must have Rust.

## Introduction

"Woah, this repository is public? Does that mean I can access the materials without being part of the program?"

Yes and no, respectively. The repository is public because it has no reason to be private (you can't access sensitive materials or information from here unless you have access). Here are some benefits as to it being public:
- I can put contribution instructions in the public README.
- Because [mast-web](https://gitlab.com/mathadvance/mast/mast-web) is going to work with the **precise** structure of this repository, open-sourcing it will make the mast-web technology more clear to outsiders.

## Cloning

If you are a staff member of MAST who wants to fetch all the materials, get a [GitLab personal access token](https://gitlab.com/-/profile/personal_access_tokens). For our purposes we will pretend that the PAT generated was `xxxx`; substitute it with your actual PAT.

	git clone https://gitlab.com/mathadvance/mast/mast-content
    cd mast-content
    echo xxxx > PAT
    sync.sh

This will fetch and build (with `latexmk`) the `master` branch of every unit.

This is meant for internal use, so if you are not a staff member of MAST, you will need to input a PAT anyway. At the very least, I have made no provisions as to what happens if you do not have a valid PAT.

## Contributor Guidelines

Please read the following if you are interested in contributing material to the MAST program.

Disclaimer: If you contribute material to MAST and we accept it, **it becomes ours**. This is just so we don't face the small risk that someone decides to take their repository down from under our noses.

Typically, I am very liberal with what contributors can do with their material (and MAST material in general). This is only possible because there's a large degree of trust involved. **If you don't trust us to do right by your material, don't contribute it.**

### New Materials

This script reads from the [units](https://gitlab.com/mathadvance/mast/units) and [diagnostics](https://gitlab.com/mathadvance/mast/diagnostics) subgroups. (These URLs are hard-coded, but any dingbat would be able to change them if needed.) So all that needs to happen is for a new repository to get added to the appropriate subgroup.

To contribute material and get it approved, here is what you should do:

1. Create a git repository with a `.tex` and `.pdf` file of your material (compiled in [mast.cls](https://gitlab.com/mathadvance/tex/mast)) and push it to your personal GitLab account. (Make sure your `.gitignore` ignores any compiled files. A sample can be found in this repository's `.gitignore`.)

2. Share your GitLab repository with me (my username is `chennis`). Please give me permission to add other people as I see fit, because I may have other MAST staff look over it as well.

3. Contact me (Dennis Chen) in some way to let me know your repository exists. One option is emailing me at [dchen@mathadvance.org](mailto:dchen@mathadvance.org).

4. Only once your material is approved, I will transfer ownership of the repository to the `mathadvance/mast/units` GitLab subgroup. 

If you don't know how to do any of these steps, that's OK. Contact me and I'll walk you through it (but do please try to use Google before you contact me, many of these steps are quite simple). If you really don't have time, I can just add your files to the right place myself.
